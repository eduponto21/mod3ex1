
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Pratica31 {
    
    private static Date inicio = new Date();
    private static String meuNome = "edUaRdo dARRAzão";
    private static String abrNome;
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1999, 3, 21);
    private static GregorianCalendar hoje = new GregorianCalendar();
    
    public static void main(String[] args) {
        
        System.out.println(meuNome.toUpperCase());
        
        abrNome = meuNome.toUpperCase().charAt(8) + meuNome.toLowerCase().substring(9);
        abrNome = abrNome + ", " + meuNome.toUpperCase().charAt(0) + ".";
        System.out.println(abrNome);
        
        long ms = hoje.getTime().getTime() - dataNascimento.getTime().getTime();
        System.out.println(ms/(1000*60*60*24));
        
        Date fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());
    }
    
}
